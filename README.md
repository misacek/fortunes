
Kompilace vyroku z Ceske republiky roku 2020. Soubor je zamysleny k pouziti s linuxovym [fortune][fortune].

Puvodnim zdrojem citatu je [clanek Deniku N][denikN].

## Zobrazeni vyroku pomoci [fortune][fortune]

    $ export FORTUNE_DIR=~/.local/share/games/fortune
    $ mkdir -p ${FORTUNE_DIR}
    $ curl https://gitlab.com/misacek/fortunes/raw/master/2020-czech-gov-fails > ${FORTUNE_DIR}/2020-czech-gov-fails
    $ strfile  ${FORTUNE_DIR}/2020-czech-gov-fails

```

$ fortune ${FORTUNE_DIR}/2020-czech-gov-fails

Vážená paní poslankyně, já jsem si myslel, že to je náhoda, že si v tom
libujete. Vy jste skutečně nechutná. Vám nevadí, že tady jsou nějací pozůstalí?
Vy stále mluvíte o těch mrtvých. Vám to dělá radost, že tolik lidí zemřelo? Asi
jo, když to tady stále dáváte na stůl. A vytrhujete z kontextu nějaké věci. Že
jste to neřešila někdy v červnu, v červenci. Teď to řešíte. Čím hůř pro Českou
republiku a pro občany, tím líp, pokud jde o pirátskou stranu. To vám dělá
radost, že jo?

Andrej Babiš, předseda vlády, reaguje na interpelaci poslankyně Olgy Richterové (Piráti), 12. 11. 2020
```

```
$ fortune ${FORTUNE_DIR}/2020-czech-gov-fails | cowsay -f turkey
 ________________________________________
/  Někdo se podivoval, jak to, že      \
| nenosím roušku. O mně je           |
| všeobecně známo, že na lyžích    |
| nenosím helmu. To je taková moje     |
| polemika s takzvaným principem       |
| předběžné opatrnosti, který přes |
| zelenou ideologii ovládl náš svět. |
|                                        |
| Václav Klaus (Prof., Ing., CSc.),     |
| bývalý prezident České republiky,  |
\ 27. 3. 2020                           /
 ----------------------------------------
  \                                  ,+*^^*+___+++_
   \                           ,*^^^^              )
    \                       _+*                     ^**+_
     \                    +^       _ _++*+_+++_,         )
              _+^^*+_    (     ,+*^ ^          \+_        )
             {       )  (    ,(    ,_+--+--,      ^)      ^\
            { (@)    } f   ,(  ,+-^ __*_*_  ^^\_   ^\       )
           {:;-/    (_+*-+^^^^^+*+*<_ _++_)_    )    )      /
          ( /  (    (        ,___    ^*+_+* )   <    <      \
           U _/     )    *--<  ) ^\-----++__)   )    )       )
            (      )  _(^)^^))  )  )\^^^^^))^*+/    /       /
          (      /  (_))_^)) )  )  ))^^^^^))^^^)__/     +^^
         (     ,/    (^))^))  )  ) ))^^^^^^^))^^)       _)
          *+__+*       (_))^)  ) ) ))^^^^^^))^^^^^)____*^
          \             \_)^)_)) ))^^^^^^^^^^))^^^^)
           (_             ^\__^^^^^^^^^^^^))^^^^^^^)
             ^\___            ^\__^^^^^^))^^^^^^^^)\\
                  ^^^^^\uuu/^^\uuu/^^^^\^\^\^\^\^\^\^\
                     ___) >____) >___   ^\_\_\_\_\_\_\)
                    ^^^//\\_^^//\\_^       ^(\_\_\_\)
                      ^^^ ^^ ^^^ ^

```


[fortune]: https://en.wikipedia.org/wiki/Fortune_(Unix)
[denikN]: https://denikn.cz/521946/170-vyroku-ktere-vam-o-ceskem-boji-s-koronavirem-reknou-vse-co-potrebujete-vedet/?ref=list
